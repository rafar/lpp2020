<?php

namespace Lpp\Helpers;

use Lpp\Entity\Item;
use Lpp\Service\Items\ItemServiceInterface;

/**
 * Retrieves items from brans as array.
 */
class ItemsRetriever
{
    /** @var ItemServiceInterface */
    private $itemsService;

    /** @var int */
    private $collectionId;

    public function __construct(int $collectionId, ItemServiceInterface $itemsService)
    {
        $this->collectionId = $collectionId;
        $this->itemsService = $itemsService;
    }

    /**
     * @return array|Item[]
     */
    public function getItems(): array
    {
        $brands = $this->itemsService->getResultForCollectionId($this->collectionId);
        foreach ($brands as $brand) {
            $itemsPart[] = $brand->items;
        }
        if (empty($itemsPart)) {
            return [];
        }

        return array_merge(...$itemsPart);
    }
}
