<?php

namespace Lpp\Service\DataReader\JsonDataReader;

use InvalidArgumentException;
use Lpp\Service\DataReader\DataReaderInterface;
use stdClass;

class JsonDataReader implements DataReaderInterface
{
    /** @var string */
    private $rawData;

    /** @var int */
    private $collectionId;

    public function readByCollectionId(int $collectionId): self
    {
        $this->collectionId = $collectionId;
        $this->readData();

        return $this;
    }

    public function getData(): stdClass
    {
        return json_decode($this->rawData, false, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws InvalidArgumentException
     */
    private function readData(): void
    {
        $dataSource = sprintf('data/%d.json', $this->collectionId);
        if (!file_exists($dataSource)) {
            throw new InvalidArgumentException(sprintf('Source ' . $dataSource . ' doesn\'t exists.'));
        }
        $this->rawData = file_get_contents($dataSource);
    }
}
