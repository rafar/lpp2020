<?php

namespace Lpp\Service\DataReader\JsonDataReader\Validators;

class JsonPriceValidator extends AbstractJsonValidator
{
    public function validate(): void
    {
        $this->checkAttributes([
            'description' => self::STRING,
            'priceInEuro' => self::INTEGER,
            'arrival' => self::DATE,
            'due' => self::DATE,
        ]);
    }
}
