<?php

namespace Lpp\Service\DataReader\JsonDataReader\Validators;

class JsonItemValidator extends AbstractJsonValidator
{
    public function validate(): void
    {
        $this->checkAttributes([
            'name' => self::STRING,
            'url' => self::URL,
            'prices' => self::OBJECT,
        ]);
    }
}
