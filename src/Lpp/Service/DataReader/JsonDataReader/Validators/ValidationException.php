<?php

namespace Lpp\Service\DataReader\JsonDataReader\Validators;

use InvalidArgumentException;

class ValidationException extends InvalidArgumentException
{
}
