<?php

namespace Lpp\Service\DataReader\JsonDataReader\Validators;

class JsonBrandValidator extends AbstractJsonValidator
{
    public function validate(): void
    {
        $this->checkAttributes([
            'name' => self::STRING,
            'description' => self::STRING,
            'items' => self::OBJECT,
        ]);
    }
}
