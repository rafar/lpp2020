<?php

namespace Lpp\Service\DataReader\JsonDataReader\Validators;

use InvalidArgumentException;
use stdClass;

abstract class AbstractJsonValidator
{
    protected const STRING = 'string';
    protected const INTEGER = 'integer';
    protected const OBJECT = 'object';
    protected const URL = 'url';
    protected const DATE = 'date';

    /** @var stdClass */
    private $item;

    public function __construct(stdClass $item)
    {
        $this->item = $item;
        $this->validate();
    }

    /**
     * @throws InvalidArgumentException
     */
    abstract public function validate();

    /**
     * @throws ValidationException
     */
    protected function checkAttributes(array $attributes): void
    {
        foreach ($attributes as $attribute => $type) {
            if (!isset($this->item->{$attribute})) {
                throw new InvalidArgumentException('There is no ' . $attribute . ' attribute.');
            }
            $attributeValue = $this->item->{$attribute};
            switch ($type) {
                case self::STRING:
                    $this->validateString($attributeValue);

                    break;
                case self::OBJECT:
                    $this->validateObject($attributeValue);

                    break;
                case self::INTEGER:
                    $this->validateInteger($attributeValue);

                    break;
                case self::URL:
                    $this->validateUrl($attributeValue);

                    break;
                case self::DATE:
                    $this->validateDate($attributeValue);

                    break;
                default:
                    throw new InvalidArgumentException('No validation implementation of ' . $type);
            }
        }
    }

    private function validateString($attributeValue): void
    {
        if (!is_string($attributeValue)) {
            throw new ValidationException(print_r($attributeValue, true) . 'is not a string.');
        }
    }

    private function validateObject($attributeValue): void
    {
        if (!is_object($attributeValue)) {
            throw new ValidationException(print_r($attributeValue, true) . 'is not an object.');
        }
    }

    private function validateInteger($attributeValue): void
    {
        if (!is_int($attributeValue)) {
            throw new InvalidArgumentException(print_r($attributeValue, true) . 'is not a integer.');
        }
    }

    private function validateUrl($attributeValue): void
    {
        if (false === filter_var($attributeValue, FILTER_VALIDATE_URL)) {
            throw new ValidationException(print_r($attributeValue, true) . ' is not an url.');
        }
    }

    private function validateDate($attributeValue): void
    {
        if (!is_string($attributeValue)) {
            throw new ValidationException(print_r($attributeValue, true) . ' is not a date.');
        }
        $parsedDate = date_parse($attributeValue);
        if ($parsedDate['errors']) {
            throw new ValidationException(print_r($attributeValue, true) . ' is not a date. ' . implode(' ', $parsedDate['errors']));
        }
    }
}
