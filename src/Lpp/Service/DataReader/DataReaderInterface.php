<?php

namespace Lpp\Service\DataReader;

use stdClass;

interface DataReaderInterface
{
    public function getData(): stdClass;

    public function readByCollectionId(int $collectionId);
}
