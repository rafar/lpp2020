<?php

namespace Lpp\Service\Brands;

use InvalidArgumentException;
use Lpp\Entity\Item;
use Lpp\Helpers\ItemsRetriever;
use Lpp\Service\Items\ItemServiceInterface;

class ItemNameOrderedBrandService implements BrandServiceInterface
{
    private $collectionNameToIdMapping = self::COLLECTION_NAME_TO_ID_MAPPING;

    /** @var ItemServiceInterface */
    private $itemsService;

    /**
     * @param string $collectionName Name of a collection to search for
     *
     * @throws InvalidArgumentException
     *
     * @return Item[]
     */
    public function getItemsForCollection($collectionName): array
    {
        if (empty($this->collectionNameToIdMapping[$collectionName])) {
            throw new InvalidArgumentException(sprintf('Provided collection name [%s] is not mapped.', $collectionName));
        }
        $items = (new ItemsRetriever($this->collectionNameToIdMapping[$collectionName], $this->itemsService))->getItems();
        usort($items, static function (Item $item1, Item $item2) {
            return $item1->name > $item2->name;
        });

        return $items;
    }

    public function setItemService(ItemServiceInterface $itemService): self
    {
        $this->itemsService = $itemService;

        return $this;
    }
}
