<?php

namespace Lpp\Service\Brands;

use Lpp\Helpers\ItemsRetriever;
use Lpp\Service\Items\ItemServiceInterface;

class UnorderedBrandService implements BrandServiceInterface
{
    /** @var ItemServiceInterface */
    private $itemService;

    /**
     * Maps from collection name to the id for the item service.
     *
     * @var []
     */
    private $collectionNameToIdMapping = self::COLLECTION_NAME_TO_ID_MAPPING;

    public function __construct(ItemServiceInterface $itemService)
    {
        $this->itemService = $itemService;
    }

    /**
     * @param string $collectionName name of the collection to search for
     *
     * @return \Lpp\Entity\Brand[]
     */
    public function getBrandsForCollection($collectionName): array
    {
        if (empty($this->collectionNameToIdMapping[$collectionName])) {
            throw new \InvalidArgumentException(sprintf('Provided collection name [%s] is not mapped.', $collectionName));
        }

        $collectionId = $this->collectionNameToIdMapping[$collectionName];

        return $this->itemService->getResultForCollectionId($collectionId);
    }

    /**
     * This is supposed to be used for testing purposes.
     * You should avoid replacing the item service at runtime.
     */
    public function setItemService(ItemServiceInterface $itemService): void
    {
        $this->itemService = $itemService;
    }

    /**
     * @param string $collectionName Name of a collection to search for
     *
     * @return \Lpp\Entity\Item[]
     */
    public function getItemsForCollection($collectionName)
    {
        return (new ItemsRetriever($this->collectionNameToIdMapping[$collectionName], $this->itemService))->getItems();
    }
}
