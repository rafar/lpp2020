<?php

namespace Lpp\Service\Brands;

use Lpp\Entity\Item;
use Lpp\Service\Items\ItemServiceInterface;

/**
 * The implementation is responsible for resolving the id of the collection from the
 * given collection name.
 *
 * Second responsibility is to sort the returning result from the item service in whatever way.
 *
 * Please write in the case study's summary if you find this approach correct or not. In both cases explain why.
 */
interface BrandServiceInterface
{
    public const WINTER = 'winter';
    public const WINTER_ID = 1315475;

    public const COLLECTION_NAME_TO_ID_MAPPING = [
        self::WINTER => self::WINTER_ID,
    ];

    /**
     * @param string $collectionName Name of a collection to search for
     *
     * @return Item[]
     */
    public function getItemsForCollection($collectionName);

    /**
     * This is supposed to be used for testing purposes.
     * You should avoid replacing the item service at runtime.
     *
     * @param temServiceInterface $itemService
     */
    public function setItemService(ItemServiceInterface $ItemService);
}
