<?php

namespace Lpp\Service\Items;

use Lpp\Entity\Brand;
use Lpp\Entity\Item;
use Lpp\Entity\Price;
use Lpp\Service\DataReader\DataReaderInterface;
use Lpp\Service\DataReader\JsonDataReader\Validators\JsonBrandValidator;
use Lpp\Service\DataReader\JsonDataReader\Validators\JsonItemValidator;
use Lpp\Service\DataReader\JsonDataReader\Validators\JsonPriceValidator;
use stdClass;

class BasicItemService implements ItemServiceInterface
{
    /** @var DataReaderInterface */
    private $dataReader;

    public function __construct(DataReaderInterface $dataReader)
    {
        $this->dataReader = $dataReader;
    }

    /**
     * This method should read from a datasource (JSON for case study)
     * and should return an unsorted list of brands found in the datasource.
     *
     * @return array|Brand[]
     */
    public function getResultForCollectionId(int $collectionId): array
    {
        $this->dataReader->readByCollectionId($collectionId);
        $data = $this->dataReader->getData();

        foreach ($data->brands as $brandId => $brand) {
            $result[$brandId] = $this->prepareBrand($brand);
        }

        return $result ?? [];
    }

    private function prepareBrand(stdClass $brandData): Brand
    {
        (new JsonBrandValidator($brandData))->validate();

        $brand = new Brand();
        $brand->brand = $brandData->name;
        $brand->description = $brandData->description;

        foreach ($brandData->items as $itemId => $item) {
            $items[$itemId] = $this->prepareItem($item);
        }
        $brand->items = $items ?? [];

        return $brand;
    }

    private function prepareItem(stdClass $itemData): Item
    {
        (new JsonItemValidator($itemData))->validate();

        $item = new Item();
        $item->url = $itemData->url;
        $item->name = $itemData->name;

        foreach ($itemData->prices as $priceId => $price) {
            $prices[$priceId] = $this->preparePrice($price);
        }
        $item->prices = $prices ?? [];

        return $item;
    }

    private function preparePrice(stdClass $priceData): Price
    {
        (new JsonPriceValidator($priceData))->validate();

        $price = new Price();
        $price->description = $priceData->description;
        $price->priceInEuro = $priceData->priceInEuro;
        $price->arrivalDate = $priceData->arrival;
        $price->dueDate = $priceData->due;

        return $price;
    }
}
