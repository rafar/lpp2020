<?php

namespace Lpp\Tasks;

use Lpp\Service\Brands\BrandServiceInterface;
use Lpp\Service\Brands\ItemNameOrderedBrandService;
use Lpp\Service\DataReader\JsonDataReader\JsonDataReader;
use Lpp\Service\Items\BasicItemService;

class Task3 implements TaskInterface
{
    public function run(): array
    {
        $basicItemService = new BasicItemService(new JsonDataReader());

        return (new ItemNameOrderedBrandService())
            ->setItemService($basicItemService)
            ->getItemsForCollection(BrandServiceInterface::WINTER)
        ;
    }
}
