<?php

namespace Lpp\Tasks;

use Lpp\Service\Brands\BrandServiceInterface;
use Lpp\Service\DataReader\JsonDataReader\JsonDataReader;
use Lpp\Service\Items\BasicItemService;

class Task1 implements TaskInterface
{
    public function run(): array
    {
        $basicItemService = new BasicItemService(new JsonDataReader());

        return $basicItemService->getResultForCollectionId(BrandServiceInterface::WINTER_ID);
    }
}
