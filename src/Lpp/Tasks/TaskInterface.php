<?php

namespace Lpp\Tasks;

interface TaskInterface
{
    public function run(): array;
}
