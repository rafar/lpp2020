<?php

namespace Test\Lpp\Service\DataReader\Validators;

use Lpp\Service\DataReader\JsonDataReader\Validators\JsonItemValidator;
use Lpp\Service\DataReader\JsonDataReader\Validators\ValidationException;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * @internal
 * @coversNothing
 */
class JsonItemValidatorTest extends TestCase
{
    public function basicDataProvider(): array
    {
        return [
            ['name', 'http://sly.pl', new stdClass(), true],
            ['name', 'notAnUrl', new stdClass(), false],
            [11, 'http://sly.pl', new stdClass(), false],
            ['name', 'http://sly.pl', [], false],
            ['name', 'http://sly.pl', 'aaa', false],
        ];
    }

    /**
     * @dataProvider basicDataProvider
     *
     * @param mixed $name
     * @param mixed $url
     * @param mixed $prices
     */
    public function testBasic($name, $url, $prices, bool $dataIsValid): void
    {
        if (!$dataIsValid) {
            $this->expectException(ValidationException::class);
        }
        $item = new stdClass();
        $item->name = $name;
        $item->url = $url;
        $item->prices = $prices;
        (new JsonItemValidator($item))->validate();
        if ($dataIsValid) {
            self::assertTrue(true);
        }
    }
}
