<?php

use Lpp\Tasks\Task1;
use Lpp\Tasks\Task3;

require __DIR__ . '/vendor/autoload.php';

$task1Results = (new Task1())->run();
$task3Results =(new Task3())->run();

print_r($task1Results);
echo "\n".str_repeat('-', 60)."\n";
print_r($task3Results);
